module.exports = function relativeTimeTranslator(
  moment
) {

  return {
      translateRelativeUtcTimeRangeToAbsolute
  };

  /**
   * See doc of function returned within/below.
   * @param {*} param0 - object that looks like:
   * {
   *   currentUnixMoment - unix time, typically now, but exposed (useful for testing),
   *   tz - olsen value,
   *   referenceUnixMoment - unix time, pass if it's desired to account for shifts
   *     of daylight saving.  The reference time is compared to the current time and any shifts
   *     are accounted for.  For example, if the current time is 4/1/2020, and the reference
   *     time is 12/1/2020, there is a 1hour shift backwards between the two
   * }
   * @returns {Function} - function that translates relative time range, see below
   */
  function translateRelativeUtcTimeRangeToAbsolute(
      { // default values in a sparse arg
          currentUnixMoment = moment.unix(),
          tz = moment.tz.guess(),
          referenceUnixMoment
      } = { // default if no arg/object passed
          currentUnixMoment: moment.unix(),
          tz: moment.tz.guess()
      }
  ) {
      /**
       * startTime and endTime look like HH:mm:ss,
       * which are relative times.  There is a need to understand
       * these in absolute time.  for example:
       *
       * case 1:
       * start_time: "01:00:00"
       * end_time: "04:00:00"
       * (current time: 3am, 1/1/2020)
       * return value: {
       *  startTime: [2020-01-01 01:00:00+00] (in unix time),
       *  endTime: [2020-01-01 04:00:00+00] (in unix time)
       * }
       *
       * case 2 - note the shift of end time:
       * start_time: "09:00:00"
       * end_time: "02:00:00"
       * (current time: 3am, 1/1/2020)
       * return value: {
       *  startTime: [2020-01-01 09:00:00+00] (in unix time),
       *  endTime: [2020-01-02 02:00:00+00] (in unix time)
       * }
       *
       * case 3 - note the shift of start time:
       * start_time: "09:00:00"
       * end_time: "02:00:00"
       * (current time: 1am, 1/1/2020)
       * return value: {
       *  startTime: [2019-12-31 09:00:00+00] (in unix time),
       *  endTime: [2020-01-02 02:00:00+00] (in unix time)
       * }
       * @param {*} param0 - object that looks like:
       * {
       *   startTime - in HH:mm:ss format
       *   endTime - in HH:mm:ss format
       * }
       * @returns {Object} - { startTime, endTime } in unix time
       */
      return function ({startTime, endTime}) {
          // Parse the start and end time info from the DB column values. start_time and end_time are passed as HH:mm:ss, and
          // should be UTC.
          const [startHour, startMinute, startSecond] = startTime.split(`:`).map(n => parseInt(n));
          const [endHour, endMinute, endSecond] = endTime.split(`:`).map(n => parseInt(n));

          let absoluteStartTime = moment.unix(currentUnixMoment).utc().set({
              hour: startHour,
              minute: startMinute,
              second: startSecond
          });
          let absoluteEndTime = moment.unix(currentUnixMoment).utc().set({
              hour: endHour,
              minute: endMinute,
              second: endSecond
          });

          if (referenceUnixMoment) {
              // account for daylight saving shift
              absoluteStartTime = shiftTimeAsAppropriateForDaylightShifts(tz)(moment.unix(referenceUnixMoment), absoluteStartTime);
              absoluteEndTime = shiftTimeAsAppropriateForDaylightShifts(tz)(moment.unix(referenceUnixMoment), absoluteEndTime);
          }

          /*
          an example here should help.  given utc of
          start time: 9pm
          end time: 2am
          this means the window is from 9pm on a given day, till 2am of
          THE NEXT day.
          */
          if (absoluteStartTime > absoluteEndTime) {
              if (currentUnixMoment > absoluteEndTime.unix()) {
                  /*
                  continuing the example...
                  So the current time is, say, 3am, and the start time is
                  later the same say, at 9pm, as created above, relative to
                  current time. The end time should be 2am of THE NEXT day.
                  */
                  absoluteEndTime = absoluteEndTime.add(1, `d`);
              } else {
                  /*
                  if the current time is 1am, then the end time is 2am of the
                  same day, but the start time is not 9pm of the same day, but
                  of THE PREVIOUS day
                  */
                  absoluteStartTime = absoluteStartTime.subtract(1, `d`);
              }
          }

          const unixStartTime = absoluteStartTime.unix();
          const unixEndTime = absoluteEndTime.unix();

          return {
              startTime: unixStartTime,
              endTime: unixEndTime,
              toRelative: toRelative({ unixStartTime, unixEndTime })
          };
      };
  }

  // handy function to get relative time of the adjusted absolutes
  function toRelative({unixStartTime, unixEndTime, tz = `GMT`}) {
      return () => ({
          startTime: moment.unix(unixStartTime).tz(tz).format(`HH:mm:ss`),
          endTime: moment.unix(unixEndTime).tz(tz).format(`HH:mm:ss`)
      });
  }

  function shiftTimeAsAppropriateForDaylightShifts(tz) {
      return (previousDateTime, nextDateTime) => {
          const offsetDiff =
              getOffsetDiff(tz)(previousDateTime.unix(), nextDateTime.unix());

          return nextDateTime.clone().add(offsetDiff, `minutes`);
      };
  }

  function getOffsetDiff(tz) {
      return (unix1, unix2) => {
          const offset1 = moment.unix(unix1).tz(tz).utcOffset();
          const offset2 = moment.unix(unix2).tz(tz).utcOffset();

          return Math.abs(offset2) - Math.abs(offset1);
      };
  }
};
